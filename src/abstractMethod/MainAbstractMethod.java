package abstractMethod;

public class MainAbstractMethod {
    public static void main(String[] args){
        Cat cat = new Cat("Pussi",1);
        cat.run();
        cat.walk();
        cat.jump();
        cat.canMakeSound();
        cat.canEat();

        System.out.println();
        Cat cat1 = new Cat("Catty",2);
        cat1.run();
        cat1.walk();
        cat1.jump();
        cat1.canMakeSound();
        cat1.canEat();

        System.out.println();
        Cat cat2 = new Cat("Citty",3);
        cat2.run();
        cat2.walk();
        cat2.jump();
        cat2.canMakeSound();
        cat2.canEat();

        System.out.println();
        Horse horse1 = new Horse("Baidu");
        horse1.run();
        horse1.walk();
        horse1.jump();
        horse1.canMakeSound();
        horse1.canEat();
    }
}
