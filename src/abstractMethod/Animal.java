package abstractMethod;

public abstract class Animal {
    String name;
    int age;

    abstract void walk();
    abstract void jump();
    abstract void canMakeSound();
    abstract void run();
    abstract void canEat();

}
