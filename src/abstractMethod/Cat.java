package abstractMethod;

public class Cat extends Animal{

    public Cat(String name, int age){
        this.name = name;
        this.age = age;
    }

    @Override
    void walk() {
        System.out.println("Cat can walk");
    }

    @Override
    void jump() {
        System.out.println("Cat can jump");
    }

    @Override
    void canMakeSound() {
        System.out.println("Cat can canMakeSound");
    }

    @Override
    void run() {
        System.out.println("The Cat "+ name+", can run and age "+age);
    }

    @Override
    void canEat() {
        System.out.println("Cat can eat meal");
    }

}
