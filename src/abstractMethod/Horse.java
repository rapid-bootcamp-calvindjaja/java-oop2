package abstractMethod;

public class Horse extends Animal{

    public Horse(String name){
        this.name = name;
    }

    @Override
    void walk() {
        System.out.println("Horse can walk");
    }

    @Override
    void jump() {
        System.out.println("Horse can jump");
    }

    @Override
    void canMakeSound() {
        System.out.println("Horse can canMakeSound");
    }

    @Override
    void run() {
        System.out.println("The Horse "+ name+", can run");
    }

    @Override
    void canEat() {
        System.out.println("Horse can eat meal");
    }
}
