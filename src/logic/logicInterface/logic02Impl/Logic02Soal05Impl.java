package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic02Soal05Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal05Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int n = this.logic.n;
        int tempArray[] = new int[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j <= 2) tempArray[j] = 1;
                else tempArray[j] = tempArray[j - 1] + tempArray[j - 2] + tempArray[j - 3];

                if (j >= i && j >= n - i - 1
                        || j <= i && j <= n - i - 1) {
                    this.logic.array[i][j] = String.valueOf(tempArray[j]);
                }
            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
}

