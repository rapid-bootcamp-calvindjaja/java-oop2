package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal06Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal06Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int flagArray = 0;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            int bil = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    bil = bil + 1;
                }
            }
            if (bil == 2) {
                this.logic.array[0][flagArray] = String.valueOf(i);
                flagArray++;
            }
            if (flagArray >= this.logic.array.length) {
                break;
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.logic.printSingle();
    }
}
