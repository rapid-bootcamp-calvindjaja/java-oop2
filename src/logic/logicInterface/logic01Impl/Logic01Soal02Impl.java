package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal02Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal02Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray() {
        int deretPertama = 0;
        int deretKedua = 0;
        for (int i = 0; i < this.logic.n; i++) {
            if (i % 2 == 0) {
                deretPertama++;
                this.logic.array[0][i] = String.valueOf(deretPertama);
            } else {
                deretKedua += 3;
                this.logic.array[0][i] = String.valueOf(deretKedua);
            }
        }
    }

    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
