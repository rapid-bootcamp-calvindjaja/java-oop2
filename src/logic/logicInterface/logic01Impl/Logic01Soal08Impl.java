package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal08Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal08Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray() {
        int[] deret = new int[this.logic.n];
        int bilPertama = 65;
        int bilKedua = 2;
        for (int i = 0; i < this.logic.n; i++) {
            if (i % 2 == 1) {
                deret[i] = bilPertama;
                bilPertama += 2;
                this.logic.array[0][i] = String.valueOf((char) deret[i]);
            } else {
                deret[i] = bilKedua;
                bilKedua += 2;
                this.logic.array[0][i] = String.valueOf(deret[i]);
            }
        }
    }

    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
